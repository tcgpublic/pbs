#!/bin/bash - 
#===============================================================================
#
#           FILE: bootstrap.sh
# 
#          USAGE: ./bootstrap.sh 
# 
#    DESCRIPTION: Bootstrap installation of open source Puppet agent
#                 on a Linux server.
# 
#        OPTIONS: ---
#   REQUIREMENTS: `hostname` and `hostname -f` should return correct 
#                 values on the server
#           BUGS: ---
#          NOTES: Not tested extensivley with multihomed servers
#         AUTHOR: Alex Tayts (at), atayts@stanford.edu
#   ORGANIZATION: Stanford University
#        CREATED: 05/23/2022 17:03
#       REVISION: 25
#
#           TODO: - 
#
#===============================================================================

# base location of the script
BASE="https://code.stanford.edu/tcgpublic/pbs/raw/master"

# puppet masters
PUPPET_MASTERS="puppet.infra.stanford.edu:8140"

# puppet agent
PUPPET_AGENT_VER='7.33.0-1'

#===============================================================================

# Errors and warnings.
function in_red () {
        tput setaf 1; echo "$@"; tput sgr0
}
# Informational
function in_yellow () {
        tput setaf 3; echo "$@"; tput sgr0
}
# Success
function in_green () {
        tput setaf 2; echo "$@"; tput sgr0
}

# Check host name first thing
if ! [[ "$(hostname)" =~ [^\.]+\.[^\.]+$ ]]; then
    in_red "Host name must be set to server's FQDN. Please fix it before running the script."
    exit 0
fi

# remove packages brought in by stanford-server
function remove_stanford_server () {
    ss_status=$(/usr/bin/dpkg-query --show --showformat='${db:Status-Status}' stanford-server)
    if [ "$ss_status" == "installed" ]; then
        in_yellow "Removing extraneous packages brought in by stanford-server package..."
        pkg_to_remove_list_full="stanford-server newsyslog augeas-lenses daemontools fonts-lato javascript-common libalgorithm-c3-perl libappconfig-perl libarchive13 libaugeas0 libavahi-client3 libavahi-common-data libavahi-common3 libb-hooks-endofscope-perl libbit-vector-perl libcache-cache-perl libcarp-clan-perl libclass-c3-perl libclass-c3-xs-perl libclass-load-perl libclass-load-xs-perl libclass-tiny-perl libconfig-simple-perl libcrypt-passwdmd5-perl libcups2 libdate-calc-perl libdate-calc-xs-perl libdbi1 libdevel-caller-perl libdevel-globaldestruction-perl libdevel-lexalias-perl libdevel-overloadinfo-perl libdevel-partialdump-perl libdevel-stacktrace-perl libdist-checkconflicts-perl liberror-perl libeval-closure-perl libfile-tail-perl libfreeipmi16 libio-interactive-perl libipc-sharelite-perl libjs-jquery libldb1 libmodule-runtime-conflicts-perl libmoose-perl libmro-compat-perl libnamespace-autoclean-perl libnamespace-clean-perl libnet-openssh-perl libnet-snmp-perl libopenipmi0 libpackage-deprecationmanager-perl libpackage-stash-perl libpackage-stash-xs-perl libpadwalker-perl libperl6-export-perl libperl6-slurp-perl libpq5 libradcli4 libruby2.3 libsensors4 libsmbclient libsnmp-base libsnmp30 libstanford-certtools-perl libstanford-infrared-perl libstanford-orange-util-perl libsub-exporter-progressive-perl libsub-identify-perl libsys-hostname-long-perl libtalloc2 libtdb1 libterm-readkey-perl libtevent0 libtirpc1 libvariable-magic-perl libwbclient0 libxslt1.1 libyaml-0-2 libyaml-tiny-perl monitoring-plugins-basic monitoring-plugins-common monitoring-plugins-standard nagios-plugins-standard openipmi python-crypto python-ldb python-samba python-talloc python-tdb rake rpcbind ruby ruby-augeas ruby-deep-merge ruby-did-you-mean ruby-minitest ruby-net-telnet ruby-nokogiri ruby-pkg-config ruby-power-assert ruby-rgen ruby-safe-yaml ruby-selinux ruby-shadow ruby-test-unit ruby2.3 rubygems-integration samba-common samba-common-bin samba-libs smbclient snmp tmpreaper unzip zip"

        # filter the list above to the packages really installed
        # on this particular server
        pkg_to_remove_list=''
        for pkg in $pkg_to_remove_list_full; do
            if dpkg --status "$pkg" &> /dev/null; then
                pkg_to_remove_list="$pkg_to_remove_list $pkg"
            fi
        done
    fi
    apt-get -y purge "$pkg_to_remove_list"

	# remove various obsolete files deployed by FAI
	rm /etc/cron.daily/tripwire
}



# get information about the distro and version
if [ -e /etc/os-release ]; then
    distro=$(grep -E "^ID=" /etc/os-release | cut -d '=' -f 2 | tr -d '" ')
    version=$(grep -E "^VERSION_ID=" /etc/os-release | cut -d '=' -f 2 | tr -d '" ')
elif [ -e /etc/rocky-release ]; then
    distro="rocky"
    version=$(tr -dc '0-9.' < /etc/rocky-release | cut -d\. -f 1)
elif [ -e /etc/centos-release ]; then
    distro="centos"
    version=$(tr -dc '0-9.' < /etc/rocky-release | cut -d\. -f 1)
elif [ -e /etc/redhat-release ]; then
    distro="rhel"
    version=$(tr -dc '0-9.' < /etc/rocky-release | cut -d\. -f 1)
fi

declare -A distro_names=( ["20.04"]="focal" ["22.04"]="jammy" ["24.04"]="noble" ["10"]="buster" ["11"]="bullseye" ["12"]="bookworm" )


# check if curl is installed. if it is not, we can't go forward
if [ ! -e /usr/bin/curl ]; then
    in_red "Curl is not installed, cannot go any further."
    exit 1
fi

# check where the VM is hosted
if grep -i 'Amazon' /sys/class/dmi/id/modalias >/dev/null; then
    hosting='aws'
    sources_suffix="_aws"
elif grep 'Google' /sys/class/dmi/id/product_name >/dev/null; then
    hosting='gcp'
    sources_suffix="_gcp"
elif grep 'Microsoft Corporation' /sys/class/dmi/id/sys_vendor >/dev/null; then
    hosting='azure'
    sources_suffix="_azure"
else
    hosting='stanford'
    sources_suffix=""
fi


if [ $hosting == 'stanford' ]; then
    in_green "The server is hosted on-premises."
    in_green "Testing if code.stanford.edu is accessible from this server..."
    if ! curl -ks --connect-timeout 1 https://code.stanford.edu > /dev/null; then
        in_yellow "code.stanford.edu is not accessible from this server, setting proxy..."

        # cannot access code.stanford.edu. try using tcg proxy
        export http_proxy='http://tcg-proxy.stanford.edu:17123'
        export https_proxy='http://tcg-proxy.stanford.edu:17123'
        
        # test again
        if ! curl -ks --connect-timeout 1 https://code.stanford.edu > /dev/null; then
            in_red "Unable to acces code.stanford.edu through proxy. Cannot go any further."
            in_red "Please allow connections from this server in the tcg-proxy firewall and try again."
            in_red "Exiting."
            exit 1
        else
            in_yellow "Using tcg-proxy to access code.stanford.edu."
        fi
    else
        in_green "Successfully connected to code.stanford.edu, continuing..."
    fi
    
else
    in_green "The server is hosted in ${hosting} cloud."
fi

# Packages to be installed
PACKAGES="curl tcsh vim ca-certificates"
RHEL_PKGS="openssh-clients openssh-server bind-utils ${PACKAGES}"
DEBIAN_PKGS="openssh-client openssh-server debian-archive-keyring bind9-host lsb-base lsb-release dirmngr ${PACKAGES}"

# install kerberos support for on-prem servers
if [ $hosting == 'stanford' ]; then
    DEBIAN_PKGS="$DEBIAN_PKGS remctl-client remctl-server wallet-client kstart krb5-user"
    RHEL_PKGS="$RHEL_PKGS krb5-workstation remctl-client remctl-server kstart wallet-client"
fi

in_green "Operating system is ${distro} ${version}."

case $distro in

    'debian'|'ubuntu')
        # Disto version name
        code_name="${distro_names[$version]}"
        sources="${distro}_${code_name}${sources_suffix}"
        family='debian'
        in_green "Code name: ${code_name}"

        # Check if we can get to the sources file. It can be in the old or new format. If we cannot,
        # then exit.
        if [ "$(curl -s -o /dev/null -w '%{http_code}' -I -X GET "${BASE}/${sources}.list")" -eq 200 ]; then
            sources_type='list'
        elif [ "$(curl -s -o /dev/null -w '%{http_code}' -I -X GET "${BASE}/${sources}.sources")" -eq 200 ]; then
            sources_type='sources'
        else
            # Source file not found.
            in_red "Bootstrap does not have '${sources}.list' or '${sources}.sources' file yet. Create one!"
            in_red "Unable to continue without any repos defined. Quitting..."
            exit 1
        fi

        # make sure gnupg is installed to import the keys
        apt-get update
        apt-get -y install gnupg

        # install keys
        if [ ! -f /usr/share/keyrings/stanford-keyring.gpg ]; then
            curl -ks "${BASE}/stanford-apt-key.pem" | gpg --dearmor | gpg --import --no-default-keyring --keyring /tmp/stanford.gpg
            gpg --no-default-keyring --keyring /tmp/stanford.gpg --export --output /usr/share/keyrings/stanford-keyring.gpg
        fi
        if [ ! -f /usr/share/keyrings/stanford-tcg-keyring.gpg ]; then
            curl -ks "${BASE}/tcgdeb-key.txt" | gpg --dearmor | gpg --import --no-default-keyring --keyring /tmp/tcg.gpg
            gpg --no-default-keyring --keyring /tmp/tcg.gpg --export --output /usr/share/keyrings/stanford-tcg-keyring.gpg
        fi
        
        # remove all stock os repos which came with the installer
        if [ -f /etc/apt/sources.list ]; then
            rm /etc/apt/sources.list
        fi

        # Get sources list apropriate for the distro and
        # hosting location
        if ! [ -f /etc/apt/sources.list.d/bootstrap.${sources_type} ]; then
            rm /etc/apt/sources.list.d/*
            in_yellow "Downloading ${distro}_${code_name}${sources_suffix}.${sources_type}..."
            curl -ks -o /etc/apt/sources.list.d/bootstrap.${sources_type} "${BASE}/${sources}.${sources_type}"
        fi

        # Update apt cache
        apt-get -y update

        # Install minimum necessary packages
        apt-get -y install "${DEBIAN_PKGS}"

        # Uninstall stanford-server package with all its dependencies
        # after FAI deployment
        if [ $hosting == 'stanford' ]; then
            remove_stanford_server
        fi

        # Check if puppet is already installed
        dpkg -l puppet-agent >/dev/null 2>&1
        puppet_installed=$?

        # if nftables supported, suggest them
        case $code_name in
            'buster' | 'bullseye' | 'bookworm')
                FIREWALL_DEFAULT='nftables'
            ;;
            'jammy')
                FIREWALL_DEFAULT='nftables'
            ;;
            *)
                FIREWALL_DEFAULT='iptables'
            ;;
        esac

        # sync up time.
        # on bookworm systemd-timesyncd is used as SNTP client.
        if [ ! "$code_name" == 'bookworm' ]; then
            apt-get -y install ntpdate
            ntpdate -u time.stanford.edu
        fi

        # we do not need cloud-init on the on-prem servers.
        if [ $hosting == 'stanford' ]; then
            apt-get -y purge cloud-init
        fi
        ;;

    'centos'|'rhel'|'redhat'|'ol'|'rocky')
        # Distro major version
        code_name=$(echo "$version" | cut -d '.' -f 1)
        sources="${distro}_${code_name}${sources_suffix}"
        family='redhat'
        in_green "Major version: ${code_name}"

        # Check if we can get to the sources file. If we cannot,
        # then exit.
        if [ "$(curl -s -o /dev/null -w '%{http_code}' -I -X GET "${BASE}/${sources}.repo")" -ne 200 ]; then
            in_red "Bootstrap does not have ${sources}.repo file yet. Create one!"
            in_red "Unable to continue without any repos defined. Quitting..."
            exit 1
        fi

        # Check if we have access to private on-prem repos if the server is on-prem
        if [ $hosting == 'stanford' ]; then
            onprem_tcg_repo="https://yum.stanford.edu/repos/stanford/stanford-tcg-EL${code_name}/stanford-tcg-EL${code_name}-x86_64/repodata/repomd.xml"
            if [ "$(curl -s -o /dev/null -w '%{http_code}' -I -X GET "${onprem_tcg_repo}")" -ne 200 ]; then
                in_red "Bootstrap does not have access to TCG repo. Be sure to grant it before continuing."
                in_red "Unable to continue without access to TCG repo. Quitting..."
                exit 1
            fi
        fi

        # Get sources list apropriate for the distro and
        # hosting location
        if [ ! -f /etc/yum.repos.d/bootstrap.repo ]; then
            # remove all stock os repos which came with the installer
            # unless it is genuine RHEL
            find /etc/yum.repos.d -iname '*.repo' -not -path '*redhat.repo' -delete
            in_yellow "Downloading ${sources}.repo..."
            curl -ks -o /etc/yum.repos.d/bootstrap.repo "${BASE}/${sources}.repo"
        fi

        # Clear yum cache
        yum clean all

        # Install minimum necessary packages
        yum install -y "${RHEL_PKGS}"

        # Check if puppet is already installed
        rpm -q puppet-agent >/dev/null 2>&1
        puppet_installed=$?

        # we never run firewalld
        systemctl disable firewalld
        systemctl stop firewalld
        systemctl mask firewalld

        if [ "$code_name" == '8' ] || [ "$code_name" == '9' ]; then
            FIREWALL_DEFAULT='nftables'
            dnf install -y python3
        else
            FIREWALL_DEFAULT='nftables'
        fi

        # Make sure CodeReady Builder repo is enabled on rhel
        if [ "$distro" == 'redhat' ] && [ "$code_name" == '9' ]; then
            /usr/sbin/subscription-manager repos --enable=codeready-builder-for-rhel-9-x86_64-rpms
        fi

        # sync up time
        if [ "$code_name" == '6' ]; then
            yum install -y ntpdate
            ntpdate -u time.stanford.edu
        else
            chronyd -q 'server time.stanford.edu iburst'
        fi
        ;; 

    *)
        in_red "Unknown distribution ${distro}."
        exit 1
        ;;
esac

# good kerberos config file
curl -k -o /etc/krb5.conf "${BASE}/krb5.conf"
chown root:root /etc/krb5.conf 
chmod 0644 /etc/krb5.conf

# safety?
if [ -f /root/.k5login ]; then
    chown root:root /root/.k5login
    chmod 0600 /root/.k5login
fi

# get host keytab if we are not in the cloud
if [ $hosting == 'stanford' ]; then
    if klist -kt | grep "host/$(hostname -f)" > /dev/null; then
        in_yellow "Host keytab exists. Continuing..."
    else
        if ! which wallet; then
            in_red "Cannot get a keytab without wallet. You can either install wallet or install the keytab manually."
            exit 1
        fi                 

        failure=true
        while $failure; do 
            in_yellow "Host keytab does not exist. Trying to obrain a keytab."
            read -r -p "Enter the principal to use to obtain keytabs (or abort/skip) ---> " princ
            if [ "$princ" == "abort" ]; then
                    in_red "Cannot continue without the host keytab. Exiting..."
                    exit 1
            elif [ "$princ" == "skip" ]; then
                    in_red "OK, skipping keytab installation. Do not forget to install it manually."
                    break
            fi
            /usr/bin/kinit -F "${princ}"
            if /usr/bin/wallet -f /etc/krb5.keytab get keytab host/"$(hostname -f)"; then
                    failure=false
                    kdestroy
            fi
        done
    fi
else
    in_red "Since the server is hosted in ${hosting}, it is unable to acquire host keytab." 
    in_red "Please be sure to install the host keytab manually, otherwise kerberos authentication would not work!"
    in_red "For the same reason unable to download host-specific duo configuration."
    in_red "Please substitute the default duo keys with real ones after the first clean puppet run!"
fi

#########################################
# PUPPET INSTALLATION AND CONFIGURATION
#########################################

# bootstrap puppet installation if not installed yet
if [ $puppet_installed -ne 0 ]; then
    in_green "Installing puppet agent. This may take a bit..."

    # Rely on puppet-agent package being in TCG repos
    case $distro in
        'debian'|'ubuntu')
            apt install -y puppet-agent="${PUPPET_AGENT_VER}${code_name}"
        ;;
        'centos'|'rhel'|'redhat'|'ol'|'rocky')
            yum install -y "puppet-agent-${PUPPET_AGENT_VER}.el${code_name}"
        ;;
    esac

    # stop puppet right away as we haven't signed a cert yet
    /usr/bin/systemctl stop puppet

    # create symlinks for puppet and facter
    mkdir -p /usr/local/bin
    ln -s /opt/puppetlabs/bin/puppet /usr/local/bin/puppet
    ln -s /opt/puppetlabs/bin/facter /usr/local/bin/facter
else
    in_yellow "Puppet is already installed."
fi

# Request the information about the su_group and
# puppet environment for the server.
SU_GROUP_DEFAULT='g_tcg'
PUPPET_ENV_DEFAULT='pp_tcg_stable202403'

# lower-cased hostname
CERTNAME_DEFAULT=$(hostname -f | awk '{print tolower($0)}')

read -r -e -i "${CERTNAME_DEFAULT}" -p "Enter puppet certificate name [${CERTNAME_DEFAULT}]: " CERTNAME <&2
read -r -e -i "${SU_GROUP_DEFAULT}" -p "Enter SU group(s) separated by spaces [${SU_GROUP_DEFAULT}]: " SU_GROUP <&2
read -r -e -i "${PUPPET_ENV_DEFAULT}" -p "Enter Puppet environment [${PUPPET_ENV_DEFAULT}]: " PUPPET_ENV <&2
read -r -e -i "${FIREWALL_DEFAULT}" -p "Which firewall are you planning to use (nftables/iptables) [${FIREWALL_DEFAULT}]: " FIREWALL <&2

# configure firewall
case $distro in
    'debian'|'ubuntu')
        case $code_name in
            'buster'|'bullseye'|'bookworm'|'jammy')
                if [ "$FIREWALL" = "iptables" ]; then
                    apt-get -y install iptables
                    update-alternatives --set iptables /usr/sbin/iptables-legacy
                    update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
                    update-alternatives --set arptables /usr/sbin/arptables-legacy
                    update-alternatives --set ebtables /usr/sbin/ebtables-legacy
                else
                    # on buster nftables not installed, but iptables
                    # use netfilter backend by default.
                    if [ "$code_name" == 'bullseye' ] || [ "$code_name" == 'bookworm' ]; then
                        apt-get -y remove iptables libip6tc2 libnetfilter-conntrack3 libnfnetlink0
                    fi
                    apt-get -y install nftables
                    systemctl enable nftables
                    systemctl start nftables
                fi
            ;;
        esac
    ;;
    'centos'|'rhel'|'redhat'|'ol')
        # configuration is needed only startin EL8
        # before that there has not been an option for nftables
        if [ "$code_name" == '8' ] || [ "$code_name" == '9' ]; then
            if [ "$FIREWALL" == 'iptables' ]; then
                dnf install -y iptables-services
                systemctl disable nftables
                systemctl stop nftables
                systemctl mask nftables
                systemctl start iptables
                systemctl enable iptables
            else
				dnf remove -y iptables-services
                systemctl start nftables
                systemctl enable nftables
            fi
        fi
    ;;
    *)
        # do nothing for ubuntu
    ;;
esac


# Put the su_group fact in place before the first puppet run.
# This is our take on node classification.
mkdir -p /etc/puppetlabs/facter/facts.d
mkdir -p /etc/puppetlabs/puppet
echo -e "---\nsu_group:\n" > /etc/puppetlabs/facter/facts.d/tcg.yaml
for group in "${SU_GROUP[@]}"; do
    echo -e "- ${group}\n" >> /etc/puppetlabs/facter/facts.d/tcg.yaml 
done

# configure the agent to use proxy if no direct internet access
if [ -n "$http_proxy" ]; then
    PROXY_CONFIG="http_proxy_host = tcg-proxy.stanford.edu
http_proxy_port = 17123"
else
    PROXY_CONFIG=""
fi

cat <<PUPPETCONF > /etc/puppetlabs/puppet/puppet.conf
# Puppet configuration file.
#   https://puppet.com/docs/puppet/7/configuration.html
# Created by the bootstrap script.
#   https://code.stanford.edu/tcgpublic/pbs

[main]
certname = ${CERTNAME}

[agent]
environment = ${PUPPET_ENV}
server_list = ${PUPPET_MASTERS}
${PROXY_CONFIG}
PUPPETCONF


# Run agent in test mode to submit a certificate request
# to a puppet master.
/opt/puppetlabs/bin/puppet agent -t

# next steps
in_yellow ""
in_yellow "Next steps:"
in_yellow "    1. Sign the certificate on puppet master. On your computer run:"
in_yellow "       $ kremctl puppetca sign puppet-prod -- ${CERTNAME}"
in_yellow "    "
in_yellow "    2. Run puppet agent few times until you get a clean run:"
in_yellow "       # puppet agent --no-daemonize --verbose --onetime"
in_yellow ""

exit

