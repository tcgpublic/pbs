Puppet Bootstrap
==================

The script to bootstrap enrollment in Puppet infrastructure. The script 
is designed to be run directly from code.stanford.edu by piping to bash or only 
bootstrap.sh downloaded and run from the server:

```
curl -ks https://code.stanford.edu/tcgpublic/pbs/raw/master/bootstrap.sh | bash
```

```
curl -kLO https://code.stanford.edu/tcgpublic/pbs/raw/master/bootstrap.sh
```

or if the host is on a network without access to the internet,

```
curl -ks -x tcg-proxy.stanford.edu:17123 https://code.stanford.edu/tcgpublic/pbs/raw/master/bootstrap.sh | bash
```

```
curl -kLO -x tcg-proxy.stanford.edu:17123 https://code.stanford.edu/tcgpublic/pbs/raw/master/bootstrap.sh
```

```
curl -kLO -x vast-prxi.stanford.edu:3128 https://code.stanford.edu/tcgpublic/pbs/raw/master/bootstrap.sh
```

The script performs the following functions:

1. Determines if a server is hosted on-premises or in the cloud and which cloud in particular.
2. Determines the distribution and version of the operating sytem.
3. Depending on the hosting, distribution, version and location configures the software repositories.
4. Installs Stanford and TCG repository certificates.
5. Depending on a distribution installs minimum necessary set of packages.
6. If a server is hosted on-premises installs kerberos support, wallet, remctl and k5start.
7. Configures the iptables/nftables firewall packages.
8. Installs Puppet agent.
9. Creates Puppet configuration file.
10. Requests a certificate from a Puppet Master.

When run, the script asks a user for input of two items:

- **Enter puppet certificate name**: enter the name of the certificate. The default is an fqdn of the server.
- **Enter SU group**: enter the name of the `su_group` the server belongs to. `su_group` is used to define groupings of the servers by functionality. It is saved on a host as an external fact. Note, that puppet code for the server also must have this fact defined, otherwise the fact would be deleted and the server left without a group, which would likely make configuration unsuccessful. By default TCG group `g_tcg` is offered.
- **Enter Puppet environment**: enter the name of the environment, which you want to put the server on. By default the latest stable is offered.
- **Which firewall are you planning to use (nftables/iptables)**: makes initial configuration of the firewall. `nftables` is a default, but beware that it is not yet supported by Docker. Use `iptables` for a traditional firewall.

Notes
-----

* If a server does not have outbound connection to the internet and hence code.stanford.edu, the script tries to use tcg-proxy to get the necessary files. If that fails, the script bails out with an error message.
* If puppet-agent package is already installed on the server, the script won't reinstall it, but would overwrite the agent's configuration file.
